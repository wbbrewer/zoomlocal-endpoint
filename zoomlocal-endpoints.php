<?php

/**
 * Plugin Name: ZoomLocal Rest API Endpoints v2
 * Plugin URI: http://www.zoomlocal.com
 * Description: This plugin creates custom endpoints for ZoomLocal.
 * Version: 1.3
 * Author: Aric Brown
 * Author URI: http://www.zoomlocal.com
 * License: GPL2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class ZoomLocal_v2_Routes extends WP_REST_Controller {

    /**
     * Register the routes for the objects of the controller.
     */
    public function register_routes() {
        $version = '2';
        $namespace = 'rest/v' . $version;
        $all = 'all';
        $listing = 'listing';
        $event = 'event';

        $posts_args = array(
            'context' => array(
                'default' => 'view',
            ),
            'page' => array(
                'default' => 1,
                'sanitize_callback' => 'absint',
            ),
            'per_page' => array(
                'default' => 10,
                'sanitize_callback' => 'absint',
            ),
            'filter' => array(),
            'featured' => array(
                'sanitize_callback' => 'sanitize_text_field',),
        );
        register_rest_route($namespace, '/' . $all, array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_all_items'),
                'args' => $posts_args,
            ),
        ));
        register_rest_route($namespace, '/' . $listing, array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_listing_items'),
                'args' => $posts_args,
            ),
        ));
        register_rest_route($namespace, '/' . $listing . '/(?P<id>[\d]+)', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_item'),
                'args' => array(
                    'context' => array(
                        'default' => 'view',
                    ),
                ),
            ),
        ));
        register_rest_route($namespace, '/' . $event, array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_event_items'),
                'args' => $posts_args,
            ),
        ));
        register_rest_route($namespace, '/' . $event . '/(?P<id>[\d]+)', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_item'),
                'args' => array(
                    'context' => array(
                        'default' => 'view',
                    ),
                ),
            ),
        ));
        $query_params = $this->get_collection_params();
        register_rest_route($namespace, '/' . 'vendor', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_vendors'),
                'args' => $query_params,
            ),
        ));
    }

    /**
     * Get a collection of items
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function get_all_items($request) {
        if (isset($request['featured']) && !empty($request['featured'])) {
            $args = array(
                'post_type' => 'product',
                'meta_query' => array(
                    array(
                        'key' => '_featured',
                        'value' => $request['featured'],
                        'compare' => '='
                    )
                ),
            );
        } else {
            $args = array(
                'post_type' => 'product',
            );
        }

        $args['paged'] = $request['page'];
        $args['posts_per_page'] = $request['per_page'];

        if (is_array($request['filter'])) {
            $args = array_merge($args, $request['filter']);
            unset($args['filter']);
        }

        $query_args = $args;

        $posts_query = new WP_Query();
        $query_result = $posts_query->query($query_args);

        $data = array();
        foreach ($query_result as $item) {
            $itemdata = $this->prepare_item_for_response($item, $request);
            $data[] = $this->prepare_response_for_collection($itemdata);
        }

        $response = rest_ensure_response($data);

        $count_query = new WP_Query();
        unset($query_args['paged']);
        $query_result = $count_query->query($query_args);
        $total_posts = $count_query->found_posts;
        $response->header('X-WP-Total', (int) $total_posts);
        $max_pages = ceil($total_posts / $request['per_page']);
        $response->header('X-WP-TotalPages', (int) $max_pages);
//
//        $base = add_query_arg($request->get_query_params(), rest_url('/wp/v2/' . $this->get_post_type_base($this->post_type)));
//        if ($request['page'] > 1) {
//            $prev_page = $request['page'] - 1;
//            if ($prev_page > $max_pages) {
//                $prev_page = $max_pages;
//            }
//            $prev_link = add_query_arg('page', $prev_page, $base);
//            $response->link_header('prev', $prev_link);
//        }
//        if ($max_pages > $request['page']) {
//            $next_page = $request['page'] + 1;
//            $next_link = add_query_arg('page', $next_page, $base);
//            $response->link_header('next', $next_link);
//        }

        return $response;
    }

    /**
     * Get a collection of items
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function get_listing_items($request) {
        if (isset($request['featured']) && !empty($request['featured'])) {
            $args = array(
                'post_type' => 'product',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field' => 'slug',
                        'terms' => 'simple'
                    )
                ),
                'meta_query' => array(
                    array(
                        'key' => '_featured',
                        'value' => $request['featured'],
                        'compare' => '='
                    )
                ),
            );
        } else {
            $args = array(
                'post_type' => 'product',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field' => 'slug',
                        'terms' => 'simple'
                    )
                ),
            );
        }

        $args['paged'] = $request['page'];
        $args['posts_per_page'] = $request['per_page'];

        /* if(isset($request['filter']['s'])){
          $args['s'] = sanitize_text_field($request['filter']['s']);
          } */
        if (is_array($request['filter'])) {
            $args = array_merge($args, $request['filter']);
            unset($args['filter']);
        }

        $near_vendors = array();

        if (isset($request['radius']) && $request['radius'] > 0) {
            $vendors = YITH_Vendors()->get_vendors(array('enabled_selling' => true));
            $start_point = "";
            if (isset($request['latitude']) && isset($request['longitude'])) {
                $start_lat = sanitize_text_field($request['latitude']);
                $start_long = sanitize_text_field($request['longitude']);
            }
            $end_point = "";
            foreach ($vendors as $vendor) {
                $end_point = explode(",", $vendor->geoloc);
                if ($end_point <> false && isset($end_point[1]) && isset($end_point[0])) {
                    $d = $this->zl_get_distance($start_lat, $start_long, $end_point[0], $end_point[1]);
                    if ($d < $request['radius'] && $d <> -1) {
                        $near_vendors[] = $vendor->id;
                    }
                }
            }

            $args['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_type',
                    'field' => 'slug',
                    'terms' => 'simple',
                ),
                array(
                    'taxonomy' => 'yith_shop_vendor',
                    'field' => 'id',
                    'terms' => (!empty($near_vendors)) ? $near_vendors : array('-1')
                ),
            );
        }
	    //for listings, dont show future publish dates
	    $args['date_query'] = array(
		    //uncomment code below if we only need to show posts for the last previous date
		    /*array(
			    'column' => 'post_date',
			    'after'  => '1 month ago',
		    ),*/
		    array(
			    'column' => 'post_date',
			    'before'  => array(
				    'year'  => date('Y'),
				    'month' => date('m'),
				    'day'   => date('d')
			    ),
			    'inclusive' => true,
		    )
	    );
	    //if featured parameter is not set, only get featured items unless specified with featured=yes?no
	    if (!isset($request['featured'])) {
		    $args['meta_query']=array(
			    array(
				    'key' => '_featured',
				    'value' => 'yes',
				    'compare' => '='
			    )
		    );
	    }
        $query_args = $args;

        $posts_query = new WP_Query();
        $query_result = $posts_query->query($query_args);

        $data = array();
        foreach ($query_result as $item) {
            $itemdata = $this->prepare_item_for_response($item, $request);
            $data[] = $this->prepare_response_for_collection($itemdata);
        }

        $response = rest_ensure_response($data);

        $count_query = new WP_Query();
        unset($query_args['paged']);
        $query_result = $count_query->query($query_args);
        $total_posts = $count_query->found_posts;
        $response->header('X-WP-Total', (int) $total_posts);
        $max_pages = ceil($total_posts / $request['per_page']);
        $response->header('X-WP-TotalPages', (int) $max_pages);
//
//        $base = add_query_arg($request->get_query_params(), rest_url('/wp/v2/' . $this->get_post_type_base($this->post_type)));
//        if ($request['page'] > 1) {
//            $prev_page = $request['page'] - 1;
//            if ($prev_page > $max_pages) {
//                $prev_page = $max_pages;
//            }
//            $prev_link = add_query_arg('page', $prev_page, $base);
//            $response->link_header('prev', $prev_link);
//        }
//        if ($max_pages > $request['page']) {
//            $next_page = $request['page'] + 1;
//            $next_link = add_query_arg('page', $next_page, $base);
//            $response->link_header('next', $next_link);
//        }

        return $response;
    }

    /**
     * Get one item from the collection
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function get_item($request) {
        $id = (int) $request['id'];
        $post = get_post($id);

        if (empty($id) || empty($post->ID) || $this->post_type !== $post->post_type) {
            return new WP_Error('rest_post_invalid_id', __('Invalid post ID.'), array('status' => 404));
        }

        $data = $this->prepare_item_for_response($post, $request);
        $response = rest_ensure_response($data);

        $response->link_header('alternate', get_permalink($id), array('type' => 'text/html'));

        return $response;
    }

    /**
     * Get a collection of items
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function get_event_items($request) {
	    //if no featured variable is passed, default to yes
	    $featured_value = 'yes';
	    if (isset($request['featured']) && !empty($request['featured'])) {
		    if($request['featured']==='yes' || $request['featured']==='no'){
			    $featured_value = $request['featured'];
		    }
	    }
	    $args = array(
		    'post_type' => 'product',
		    'tax_query' => array(
			    array(
				    'taxonomy' => 'product_type',
				    'field' => 'slug',
				    'terms' => 'event'
			    )
		    ),
		    //added line below, if featured is not requested, default to show only featured
		    'meta_query' => array(
			    'relation' => 'AND',
			    array(
				    'relation' => 'OR',
				    array(
					    'key' => '_rq_event_start_date',
					    'value' => date('Y-m-d'),
					    'compare' => '>=',
					    'type' => 'DATE'
				    ),
				    array(
					    'key' => '_rq_event_end_date',
					    'value' => date('Y-m-d'),
					    'compare' => '>=',
					    'type' => 'DATE'
				    ),
			    ),
			    array(
				    'key' => '_visibility',
				    'value' => 'visible',
				    'compare' => '='
			    ),
			    array(
				    'key' => '_featured',
				    'value' => $featured_value,
				    'compare' => '='
			    ),
		    ),
		    'orderby'   => 'meta_value',
		    'meta_key'  => '_rq_event_start_date',
		    'order' => 'ASC'
	    );

	     //make sure only get events with publish dates no older than 30 days and no future dates
	    $args['date_query'] = array(
		    //uncomment line below to only show posts no older than 30 days
		    /*array(
			    'column' => 'post_date',
			    'after'  => '1 month ago',
		    ),*/
		    array(
			    'column' => 'post_date',
			    'before'  => array(
				    'year'  => date('Y'),
				    'month' => date('m'),
				    'day'   => date('d')
			    ),
			    'inclusive' => true,
		    )
	    );
	    //order by listing date first, then by modified date
	   /* $args['orderby'] = array(
		    'date' => 'ASC',
		    '_changed_date' => 'DESC'
	    );*/
	    /*$args['orderby'] = array(
		    '_rq_event_start_date' => 'ASC',
		    '_rq_event_end_date' => 'ASC'
	    );*/
        $near_events = $this->zl_get_near_events($request, $args);
        if (!empty($near_events)) {
            $args['post__in'] = $near_events;
        }
        /* End Moon */



        $args['paged'] = $request['page'];
        $args['posts_per_page'] = $request['per_page'];

        if (is_array($request['filter'])) {
            $args = array_merge($args, $request['filter']);
            unset($args['filter']);
        }

        $query_args = $args;

        $posts_query = new WP_Query();
        $query_result = $posts_query->query($query_args);

        $data = array();
        foreach ($query_result as $item) {
            $itemdata = $this->prepare_item_for_response($item, $request);
            $data[] = $this->prepare_response_for_collection($itemdata);
        }

        $response = rest_ensure_response($data);

        $count_query = new WP_Query();
        unset($query_args['paged']);
        $query_result = $count_query->query($query_args);
        $total_posts = $count_query->found_posts;
        $response->header('X-WP-Total', (int) $total_posts);
        $max_pages = ceil($total_posts / $request['per_page']);
        $response->header('X-WP-TotalPages', (int) $max_pages);
//
//        $base = add_query_arg($request->get_query_params(), rest_url('/wp/v2/' . $this->get_post_type_base($this->post_type)));
//        if ($request['page'] > 1) {
//            $prev_page = $request['page'] - 1;
//            if ($prev_page > $max_pages) {
//                $prev_page = $max_pages;
//            }
//            $prev_link = add_query_arg('page', $prev_page, $base);
//            $response->link_header('prev', $prev_link);
//        }
//        if ($max_pages > $request['page']) {
//            $next_page = $request['page'] + 1;
//            $next_link = add_query_arg('page', $next_page, $base);
//            $response->link_header('next', $next_link);
//        }

        return $response;
    }

    public function zl_get_near_events($request, $args) {
        if (isset($request['radius']) && $request['radius'] > 0 && isset($request['latitude']) && isset($request['longitude'])) {
            $query = new WP_Query($args);
            $near_events = array();
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $event_lat = get_post_meta($query->post->ID, '_rq_event_lat_name', true);
                    $event_lon = get_post_meta($query->post->ID, '_rq_event_lon_name', true);
                    if (empty($event_lat) || empty($event_lon)) {
                        continue;
                    }
                    $distance = $this->zl_get_distance($request['latitude'], $request['longitude'], $event_lat, $event_lon, "m");
                    if ($distance <= $request['radius']) {
                        $near_events[] = $query->post->ID;
                    }
                }

                return (!empty($near_events)) ? $near_events : array('-1');
            }
        } else {
            return array();
        }
    }

    /**
     * Prepare the item for the REST response
     *
     * @param mixed $item WordPress representation of the item.
     * @param WP_REST_Request $request Request object.
     * @return mixed
     */
    public function prepare_item_for_response($item, $request) {

        $prepared_item['id'] = $item->ID;
        $prepared_item['date'] = $item->post_date;
        $prepared_item['link'] = get_permalink($item->ID);
        $prepared_item['modified'] = $item->post_modified;
        $prepared_item['title'] = html_entity_decode(strip_tags($item->post_title));
        $prepared_item['short_description'] = html_entity_decode(strip_tags(apply_filters('woocommerce_short_description', $item->post_excerpt)));

        /* Get the intermediate image sizes and add the full size to the array. */
        $sizes = get_intermediate_image_sizes();

        $images = array();
        ;

        /* Loop through each of the image sizes. */
        foreach ($sizes as $size) {

            /* Get the image source, width, height, and whether it's intermediate. */
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($item->ID), $size);

            if ($image) {
                $images[$size] = $image;
            }
        }

        /* Only return images if not empty. */
        $images = array_filter($images);
        if (!empty($images)) {
            $prepared_item['images'] = $images;
        }

        /* Get only allowed WooCommerce meta data. */
        $allowed = ['_featured', '_price', '_regular_price', '_sale_price', '_visibility', '_rq_event_start_date', '_rq_event_end_date', '_rq_event_start_time', '_rq_event_stop_time', '_rq_event_country_name', '_rq_event_region_name', '_rq_event_address_name', '_rq_event_zip_code', '_rq_event_lat_name', '_rq_event_lon_name'];

        foreach ($allowed as $key) {
            $meta = get_post_meta($item->ID, $key, true);
            if (!empty($meta)) {
                $allowed_meta_keys[$key] = $meta;
            }
        }

        $prepared_item['meta'] = $allowed_meta_keys;

        $product_cat = get_the_terms($item->ID, 'product_cat');
        $product_tag = get_the_terms($item->ID, 'product_tag');

        $yith_shop_vendor = get_the_terms($item->ID, 'yith_shop_vendor');

        $yith_shop_vendor = get_term_by('term_taxonomy_id', $yith_shop_vendor[0]->term_taxonomy_id, 'yith_shop_vendor');

        if ($yith_shop_vendor) {

            $data = array(
                'id' => (int) $yith_shop_vendor->term_taxonomy_id,
                'count' => (int) $yith_shop_vendor->count,
                'description' => $yith_shop_vendor->description,
                'link' => get_term_link($yith_shop_vendor),
                'name' => $yith_shop_vendor->name,
                'slug' => $yith_shop_vendor->slug,
                'taxonomy' => $yith_shop_vendor->taxonomy,
            );
            $schema = $this->get_item_schema();
            if (!empty($schema['properties']['parent'])) {
                $data['parent'] = (int) $parent_id;
            }

            $data = rest_ensure_response($data);

            $yith_shop_vendor = apply_filters('rest_prepare_term', $data, $yith_shop_vendor, $request);
        }

//        $vendor = yith_get_vendor($item->ID, 'product');
//
//        $images = getTermMetaImages($vendor->header_image);
//
//        $hours = json_decode($vendor->hours);
//
//        $vendor_meta = array(
//            'images' => $images,
//            'name' => html_entity_decode($vendor->name),
//            'description' => html_entity_decode($vendor->description),
//            'owner' => $vendor->owner,
//            'location' => $vendor->location,
//            'telephone' => $vendor->telephone,
//            'store_email' => $vendor->store_email,
//            'enable_selling' => $vendor->enable_selling,
//            'address1' => $vendor->address1,
//            'address2' => $vendor->address2,
//            'city' => $vendor->city,
//            'state' => $vendor->state,
//            'zip' => $vendor->zip,
//            'website' => $vendor->website,
//            'hours' => $hours,
//            'header_image' => $vendor->header_image,
//            'socials' => ($vendor->socials) ? $vendor->socials : '',
//            'geoloc' => $vendor->geoloc,
//        );
//
//        $yith_shop_vendor['vendor_meta'] = $vendor_meta;

        $prepared_item['terms'] = array(
            'category' => ($product_cat) ? $product_cat : [],
            'tag' => ($product_tag) ? $product_tag : [],
            'vendor' => ($yith_shop_vendor->data) ? $yith_shop_vendor->data : $null,
        );

        return $prepared_item;
    }

    /**
     * Get the query params for collections
     *
     * @return array
     */
    public function get_collection_params() {
        return array(
            'page' => array(
                'description' => 'Current page of the collection.',
                'type' => 'integer',
                'default' => 1,
                'sanitize_callback' => 'absint',
            ),
            'per_page' => array(
                'description' => 'Maximum number of items to be returned in result set.',
                'type' => 'integer',
                'default' => 10,
                'sanitize_callback' => 'absint',
            ),
            'search' => array(
                'description' => 'Limit results to those matching a string.',
                'type' => 'string',
                'sanitize_callback' => 'sanitize_text_field',
            ),
        );
    }

    protected $taxonomy = 'yith_shop_vendor';

    /**
     * Get vendors within a radius
     *
     * @param WP_REST_Request $request Full details about the request
     * @return WP_REST_Response|WP_Error
     */
    public function get_vendors($request) {
        $prepared_args = array(
            'order' => $request['order'],
            'orderby' => $request['orderby'],
            'hide_empty' => $request['hide_empty'],
            'number' => $request['per_page'],
            'search' => $request['search'],
            'enabled_selling' => true,
            'city' => $request['city'],
            'state' => $request['state'],
        );

        $prepared_args['offset'] = ( $request['page'] - 1 ) * $prepared_args['number'];

        $taxonomy_obj = get_taxonomy($this->taxonomy);

        if ($taxonomy_obj->hierarchical && isset($request['parent'])) {
            if (0 === $request['parent']) {
                // Only query top-level terms.
                $prepared_args['parent'] = 0;
            } else {
                $parent = get_term_by('term_taxonomy_id', (int) $request['parent'], $this->taxonomy);
                if ($parent) {
                    $prepared_args['parent'] = $parent->term_id;
                }
            }
        }

        // the old way
        //$query_result = get_terms($this->taxonomy, $prepared_args);
        $near_vendors = array();
        if (isset($request['radius']) && $request['radius'] > 0) {
            $vendors = YITH_Vendors()->get_vendors(array('enabled_selling' => true));
            $start_point = "";
            if (isset($request['latitude']) && isset($request['longitude'])) {
                $start_lat = sanitize_text_field($request['latitude']);
                $start_long = sanitize_text_field($request['longitude']);
            }
            $end_point = "";
            foreach ($vendors as $vendor) {
                $end_point = explode(",", $vendor->geoloc);
                if ($end_point <> false && isset($end_point[1]) && isset($end_point[0])) {
                    $d = $this->zl_get_distance($start_lat, $start_long, $end_point[0], $end_point[1]);
                    if ($d < $request['radius'] && $d <> -1) {
                        $near_vendors[$vendor->id] = $d;
                    }
                }
            }
            if (!empty($near_vendors)) {
                $temp = $near_vendors;
                asort($near_vendors);
                $prepared_args['near_vendors'] = array_keys($near_vendors);
            } else {
                $prepared_args['near_vendors'] = false;
            }
        }
        $query_result = $this->get_vendors_query($prepared_args);

        $response = array();
        foreach ($query_result as $term) {
            $data = $this->prepare_term_for_response($term, $request);
            $response[] = $this->prepare_response_for_collection($data);
        }

        $response = rest_ensure_response($response);
        unset($prepared_args['number']);
        unset($prepared_args['offset']);
        //$total_terms = wp_count_terms($this->taxonomy, $prepared_args);

        $total_terms = count($query_result);

        // wp_count_terms can return a falsy value when the term has no children
        if (!$total_terms) {
            $total_terms = 0;
        }

        $response->header('X-WP-Total', (int) $total_terms);
        $max_pages = ceil($total_terms / $request['per_page']);
        $response->header('X-WP-TotalPages', (int) $max_pages);

//        $base = add_query_arg($request->get_query_params(), rest_url('/wp/v2/terms/' . $this->get_taxonomy_base($this->taxonomy)));
//        if ($request['page'] > 1) {
//            $prev_page = $request['page'] - 1;
//            if ($prev_page > $max_pages) {
//                $prev_page = $max_pages;
//            }
//            $prev_link = add_query_arg('page', $prev_page, $base);
//            $response->link_header('prev', $prev_link);
//        }
//        if ($max_pages > $request['page']) {
//            $next_page = $request['page'] + 1;
//            $next_link = add_query_arg('page', $next_page, $base);
//            $response->link_header('next', $next_link);
//        }

        return $response;
    }

    /**
     * Get vendors list
     *
     * @param array $args
     *
     * @return Array Vendor Objects
     *
     * @since  1.0
     * @author Aric Brown <equatorapps@gmail.com>
     */
    public function get_vendors_query($args = array()) {
//        $args = wp_parse_args($args, array(
//            'enabled_selling' => '',
//            'fields' => '',
//            'pending' => '',
//        ));
//        $query_args = array(
//            'hide_empty' => false,
//            'number' => isset($args['number']) ? $args['number'] : ''
//        );
//
//        $exclude_selling = $exclude_owner = array();
        //write_log($args);

        $query_args = $args;

        // filter for enable selling
        if ('' !== $args['enabled_selling']) {
            global $wpdb;
            $query = $wpdb->prepare("SELECT DISTINCT woocommerce_term_id FROM $wpdb->woocommerce_termmeta WHERE meta_key = %s AND meta_value = %s", 'enable_selling', $args['enabled_selling'] ? 'no' : 'yes' );

            if (isset($args['owner']) && $args['owner'] === false) {
                $query .= $wpdb->prepare(" AND woocommerce_term_id NOT IN ( SELECT DISTINCT woocommerce_term_id FROM $wpdb->woocommerce_termmeta WHERE meta_key = %s AND meta_value = %s )", 'owner', '');
            }

            $query_args['exclude'] = $wpdb->get_col($query);
        }

        // filter for pending vendors
        if (!empty($args['pending']) && 'yes' == $args['pending']) {
            global $wpdb;
            $query = $wpdb->prepare("SELECT woocommerce_term_id FROM $wpdb->woocommerce_termmeta WHERE meta_key = %s AND meta_value = %s", 'pending', $args['pending']);

            $query_args['include'] = $wpdb->get_col($query);

            if (empty($query_args['include'])) {
                return array();
            }

            //write_log($query);
        }

        // filter for city
        if (!empty($args['city']) && empty($args['state'])) {
            global $wpdb;
            $query = $wpdb->prepare("SELECT woocommerce_term_id FROM $wpdb->woocommerce_termmeta WHERE meta_key = %s AND meta_value = %s", 'city', $args['city']);

            $query_args['include'] = $wpdb->get_col($query);

            if (empty($query_args['include'])) {
                return array();
            }

            //write_log($query);
        }

        // filter for state
        if (!empty($args['state'] && empty($args['city']))) {
            global $wpdb;
            $query = $wpdb->prepare("SELECT woocommerce_term_id FROM $wpdb->woocommerce_termmeta WHERE meta_key = %s AND meta_value = %s", 'state', $args['state']);

            $query_args['include'] = $wpdb->get_col($query);

            if (empty($query_args['include'])) {
                return array();
            }

            //write_log($query);
        }

        if (isset($args['near_vendors'])) {
            if ($args['near_vendors'] == false) {
                $query_args['include'] = array('-1');
            } else {
                $query_args['include'] = $args['near_vendors'];
                $query_args['orderby'] = 'include';
            }
        }


        /*
         * Note that the 'include' argument is on hierarchy
         * pending > state/city > near_vendors_list
         * near_vendors will override state/city and pending searches
         * can be later changed if need to combine
         * */
        //unset($query_args['number']);
        //unset($query_args['order']);

        $vendors = get_terms($this->taxonomy, $query_args);
        //write_log($vendors);

        if (empty($vendors) || is_wp_error($vendors)) {
            return array();
        }

        $res = array();

        foreach ($vendors as $vendor) {
            $res[] = 'ids' == $args['fields'] ? $vendor->term_id : yith_get_vendor($vendor);
        }

        return $res;
    }

    /**
     * Prepare a single term output for response
     *
     * @param obj $item Term object
     * @param WP_REST_Request $request
     */
    public function prepare_term_for_response($item, $request) {

        $parent_id = 0;
        if ($item->parent) {
            $parent_term = get_term_by('id', (int) $item->parent, $item->taxonomy);
            if ($parent_term) {
                $parent_id = $parent_term->term_taxonomy_id;
            }
        }

        $data = array(
            'id' => (int) $item->term_taxonomy_id,
            'count' => (int) $item->count,
            'description' => $item->description,
            'link' => get_term_link($item),
            'name' => $item->name,
            'slug' => $item->slug,
            'taxonomy' => $item->taxonomy,
        );
        $schema = $this->get_item_schema();
        if (!empty($schema['properties']['parent'])) {
            $data['parent'] = (int) $parent_id;
        }

        $context = !empty($request['context']) ? $request['context'] : 'view';
        $data = $this->filter_response_by_context($data, $context);
        $data = $this->add_additional_fields_to_object($data, $request);

        $data = rest_ensure_response($data);

//        $data->add_links($this->prepare_links($item));

        /**
         * Filter a term item returned from the API.
         *
         * Allows modification of the term data right before it is returned.
         *
         * @param array           $data     Key value array of term data.
         * @param object          $item     The term object.
         * @param WP_REST_Request $request  Request used to generate the response.
         */
        return apply_filters('rest_prepare_term', $data, $item, $request);
    }

    /*
     * Calculates distance
     * Snippet Source https://www.geodatasource.com/developers/php
     */

    function zl_get_distance($lat1, $lon1, $lat2, $lon2, $unit = "m") {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

}

/**
 *
 */
function v2_add_taxonomy_to_api() {
    $cat = get_taxonomy('product_cat');
    $cat->show_in_rest = true;

    $tag = get_taxonomy('product_tag');
    $tag->show_in_rest = true;

    $yith = get_taxonomy('yith_shop_vendor');
    $yith->show_in_rest = true;
}

add_action('init', 'v2_add_taxonomy_to_api', 30);

/* @param array           $data     Key value array of term data.
 * @param object          $item     The term object.
 * @param WP_REST_Request $request  Request used to generate the response.
 */

function v2_rest_prepare_term($data, $item, $request) {

    $term = $item->taxonomy;

    switch ($term) {
        case "product_cat":
            // get the thumbnail id using the queried item term_id
            $thumbnail_id = get_woocommerce_term_meta($item->term_id, 'thumbnail_id', true);

            $images = getv2TermMetaImages($thumbnail_id);

            $meta = array(
                'images' => $images,
            );

            //
            if ($meta) {
                $_data = $data->data;
                $_data['termmeta'] = $meta;
                $data->data = $_data;
            }
            break;
        case "yith_shop_vendor":
            // get the thumbnail id using the queried item term_id
            $thumbnail_id = get_woocommerce_term_meta($item->term_id, 'header_image', true);

            $images = getv2TermMetaImages($thumbnail_id);

            $vendor = yith_get_vendor($item->term_id);

            $hours = json_decode($vendor->hours);

            $socials = json_decode($vendor->socials);

            $vendor_meta = array(
                'images' => $images,
                'name' => html_entity_decode($vendor->name),
                'description' => html_entity_decode($vendor->description),
                'owner' => $vendor->owner,
                'location' => $vendor->location,
                'telephone' => $vendor->telephone,
                'store_email' => $vendor->store_email,
                'enable_selling' => $vendor->enable_selling,
                'address1' => $vendor->address1,
                'address2' => $vendor->address2,
                'city' => $vendor->city,
                'state' => $vendor->state,
                'zip' => $vendor->zip,
                'website' => $vendor->website,
                'hours' => $hours,
                'header_image' => $vendor->header_image,
                'socials' => $socials,
                'geoloc' => $vendor->geoloc,
            );

            //
            if ($vendor_meta) {
                $_data = $data->data;
                $_data['vendor_meta'] = $vendor_meta;
                $data->data = $_data;
            }
            break;
        default:
        //
    }

    return $data;
}

add_filter('rest_prepare_term', 'v2_rest_prepare_term', 10, 3);

function getv2TermMetaImages($thumbnail_id) {
    $images;

    /* Get the intermediate image sizes and add the full size to the array. */
    $sizes = get_intermediate_image_sizes();

    /* Loop through each of the image sizes. */
    foreach ($sizes as $size) {

        /* Get the image source, width, height, and whether it's intermediate. */
        $image = wp_get_attachment_image_src($thumbnail_id, $size);

        if ($image) {
            $images[$size] = $image;
        }
    }

    return $images;
}

add_action('rest_api_init', function () {
    // hook into the rest_api_init action so we can start registering routes
    $controller = new ZoomLocal_v2_Routes();
    $controller->register_routes();
});
add_filter('get_terms_orderby', 'zl_get_terms_orderby', 10, 2);

function zl_get_terms_orderby($orderby, $args) {
    if (isset($args['orderby']) && 'include' == $args['orderby']) {
        $include = implode(',', array_map('absint', $args['include']));
        $orderby = "FIELD( t.term_id, $include )";
    }
    return $orderby;
}

if (!function_exists('write_log')) {

    function write_log($log) {
        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }

}